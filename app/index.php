<?php
    include_once(__DIR__ . "/env.inc.php");
    include_once(__DIR__ . "/database.inc.php");
    
    //Con MySQL:
    include_once(__DIR__ . "/mysql.inc.php");
    
    $conexion = new MySQL(
                    MYSQL_DB_HOST, //Host.
                    MYSQL_DB_USER, //Usuario.
                    MYSQL_DB_PASSWORD, //Contraseña.
                    MYSQL_DB_DATABASE, //Base de datos.
                    MYSQL_DB_PORT, //Puerto.
                    MYSQL_DB_PERSISTENT, //Persistente.
                    MYSQL_DB_CHARSET, //Juego de caracteres.
                    MYSQL_DB_COLLATION //Collation.
    );
    
    $where = [];
    if (isset($_GET["id"]) && $_GET["id"] != "") $where[] = 'productos.idProducto = ' . (int)$_GET["id"];
    if (isset($_GET["producto"]) && $_GET["producto"] != "") $where[] = 'productos.Nombre = "' . $conexion->escape($_GET["producto"]) . '"';
    if (isset($_GET["precio"]) && $_GET["precio"] != "") $where[] = 'productos.precio = ' . (float)$_GET["precio"];
    if (isset($_GET["marca"]) && $_GET["marca"] != "") $where[] = 'marcas.Nombre = "' . $conexion->escape($_GET["marca"]) . '"';
    if (isset($_GET["categoria"]) && $_GET["categoria"] != "") $where[] = 'categorias.Nombre = "' . $conexion->escape($_GET["categoria"]) . '"';
    if (isset($_GET["presentacion"]) && $_GET["presentacion"] != "") $where[] = 'productos.presentacion = "' . $conexion->escape($_GET["presentacion"]) . '"';
    if (isset($_GET["stock"]) && $_GET["stock"] != "") $where[] = 'productos.stock = ' . (int)$_GET["stock"];
    
    $query = "select 
                productos.idProducto as id,
                productos.Nombre as producto,
                productos.precio,
                marcas.Nombre as marca,
                categorias.Nombre as categoria,
                productos.presentacion,
                productos.stock
            from productos
                inner join marcas on productos.Marca = marcas.idMarca
                inner join categorias on productos.Categoria = categorias.idCategoria";
    
    if (count($where) > 0) $query .= " where " . implode(" and ", $where);
    
    $lectura = $conexion->read($query);
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title>Ejemplo de clase</title>
        <style>
            * {
                font-family: Arial;
                margin: 0px;
                padding: 0px;
                font-size: 12pt;
            }
            
            h1 {
                font-size: 20pt;
                margin-bottom: 10px;
            }
            
            table.productos {
                margin-bottom: 20px;
                border-collapse: collapse;
            }
            
            table.productos td {
                border: 1px solid black;
            }
            
            table.productos thead td {
                background-color: #DDDDDD;
            }
            
            h3 {
                margin-bottom: 10px;
            }
            
            form p {
                margin-bottom: 10px;
            }
            
            input[type="text"],input[type="submit"] {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>Lista de productos</h1>
        </header>
        <main>
            <div>
                <table class="productos">
                    <thead>
                        <tr>
                            <td>Código</td>
                            <td>Producto</td>
                            <td>Precio</td>
                            <td>Marca</td>
                            <td>Categoría</td>
                            <td>Presentación</td>
                            <td>Stock</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (count($lectura) > 0) {
                                foreach ($lectura as $fila) {
                        ?>
                                    <tr>
                                        <td><?= htmlspecialchars($fila->id) ?></td>
                                        <td><?= htmlspecialchars($fila->producto) ?></td>
                                        <td>$ <?= htmlspecialchars($fila->precio) ?></td>
                                        <td><?= htmlspecialchars($fila->marca) ?></td>
                                        <td><?= htmlspecialchars($fila->categoria) ?></td>
                                        <td><?= htmlspecialchars($fila->presentacion) ?></td>
                                        <td><?= htmlspecialchars($fila->stock) ?></td>
                                    </tr>
                        <?php
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div>
                <h3>Buscar por:</h3>
                <form method="get">
                    <p>
                        <span>Código: </span><input type="text" name="id" />
                    </p>
                    <p>
                        <span>Producto: </span><input type="text" name="producto" />
                    </p>
                    <p>
                        <span>Precio: </span><input type="text" name="precio" />
                    </p>
                    <p>
                        <span>Marca: </span><input type="text" name="marca" />
                    </p>
                    <p>
                        <span>Categoría: </span><input type="text" name="categoria" />
                    </p>
                    <p>
                        <span>Presentación: </span><input type="text" name="presentacion" />
                    </p>
                    <p>
                        <span>Stock: </span><input type="text" name="stock" />
                    </p>
                    <p>
                        <input type="submit" value="Buscar" />
                    </p>
                </form>
            </div>
        </main>
        <footer>
            
        </footer>
    </body>
</html>
