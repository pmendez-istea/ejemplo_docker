<?php
    class MySQL extends Database {
        /*
            Clase para manipular MySQL, a través de mysqli.
            Autor: Pablo Esteban Mendez Domeneche.
        */
        
        /*
         * bool:
         */
        private $persistent; //Por defecto, false (no persistente).

        /*
         * string:
         */
        private $charset; //Charset de cotejamiento (sin valor por defecto).

        /*
         * string:
         */
        private $collate; //Collate (sin valor por defecto).

        public function __construct(string $p_host, string $p_user, string $p_password, string $p_db = "", int $p_port = 3306, bool $p_persistent = false, string $p_charset = "utf8", string $p_collate = "utf8_unicode_ci") {
            //Al generarse el objeto, intentará la conexión con MySQL:
            parent::__construct($p_host, $p_user, $p_password, $p_db, $p_port);
            $this->connect($p_host, $p_user, $p_password, $p_db, $p_port, $p_persistent, $p_charset, $p_collate);
            
        }

        private function connect(string $p_host, string $p_user, string $p_password, string $p_db = "", int $p_port = 3306, bool $p_persistent = false, string $p_charset = "utf8", string $p_collate = "utf8_unicode_ci"): bool {
            //Intenta conectar con MySQL.
            $connection = @mysqli_connect($p_host, $p_user, $p_password, $p_db, $p_port);
            if (!$connection) { //Si no conectó...
                return false;
            }
            else {
                //Si ya estaba conectado, desconecta:
                if ($this->is_connected) $this->disconnect();

                //Almacena los datos de la conexión:
                $this->host = $p_host;
                $this->user = $p_user;
                $this->password = $p_password;
                $this->db = $p_db;
                $this->port = $p_port;
                $this->persistent = $p_persistent;
                $this->is_connected = true;
                $this->link = $connection;

                //Establece el charset y el collate:
                $this->set_names($p_charset, $p_collate);

                return true; //No hubo error.
            }
        }

        public function reconnect(): bool {
            //Llama a la función conectar(), usando los valores que ya están almacenados en la clase.
            return $this->connect($this->host, $this->user, $this->password, $this->db, $this->port, $this->persistent, $this->charset, $this->collate);
        }
        
        public function disconnect() {
            //Desconecta de MySQL, liquidando el link a la conexión.
            //No tiene return.
            if ($this->is_connected) {
                //Se asegura de matar el hilo:
                $thread = @mysqli_thread_id($this->link);
                if (is_numeric($thread)) @mysqli_kill($this->link, $thread);
                @mysqli_close($this->link); //Produce la desconexión, si estaba conectado.
                $this->is_connected = false; //Deja constancia de ésto.
            }
        }

        private function set_names(string $p_charset, string $p_collate): int {
            //Establece el charset y el collate:
            $charset = $this->escape($p_charset); //Escapa el Charset.
            $collate = $this->escape($p_collate); //Escapa el Collate.
            //Ejecuta la Query:
            $result = $this->other("SET NAMES \"$charset\"" . ($collate != "" ? " COLLATE \"$collate\"" : ""));
            if ($result == 0) { //Si no hubo error...
                //Asigna los valores a las propiedades:
                $this->charset = $charset;
                $this->collate = $collate;
                //Ejecuta idénticas operaciones, pero para PHP:
                @mysqli_set_charset($this->link, $this->charset);
            }

            return $result; //Retorna el código de error.
        }

        public function db(string $db_name): int {
            //Selecciona la base de datos por defecto.
            //Devuelve el código del error que se produzca.
            @mysqli_select_db($this->link, $db_name); //Intenta la operación.
            $result = @mysqli_errno($this->link); //Obtiene el código de error.
            //Si no hubo error, asigna el valor a la propiedad:
            if ($result == 0) $this->db = $db_name;

            return $result; //Retorna el código de error.
        }

        public function read($query) {
            //Intenta efectuar cualquier consulta que devuelva un resultset.
            //Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
            //Devuelve un array asociativo o el código del error que se produzca.
            
            $result = @mysqli_query($this->link, $query);
            if (@mysqli_errno($this->link)) return @mysqli_errno($this->link); //Si hubo error, devuelve su código.
            $count = @mysqli_num_rows($result);
            if (@mysqli_errno($this->link)) return @mysqli_errno($this->link); //Si hubo error, devuelve su código.
            $return = [];
            for ($x = 0; $x < $count; $x++) {
                $return[] = @mysqli_fetch_object($result);
                if (@mysqli_errno($this->link)) return @mysqli_errno($this->link); //Si hubo error, devuelve su código.
            }

            return $return;
        }

        public function insert($query) {
            //Intenta efectuar cualquier consulta INSERT.
            //Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
            //Devuelve el INSERT ID (como array asociativo) o el código del error que se produzca.
            @mysqli_multi_query($this->link, $query);
            //Si hubo error, devuelve su código, aunque como valor negativo, para que no se confunda con el INSERT ID:
            if (@mysqli_errno($this->link)) return @mysqli_errno($this->link);

            //Devuelve el LAST_INSERT_ID():
            return array("id" => @mysqli_insert_id($this->link));
        }

        public function delete($query) {
            //Intenta efectuar cualquier consulta DELETE.
            //Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
            //Devuelve las filas afectadas (como array asociativo) o el código del error que se produzca.
            @mysqli_multi_query($this->link, $query);
            //Si hubo error, devuelve su código, aunque como valor negativo, para que no se confunda con el AFFECTED ROWS:
            if (@mysqli_errno($this->link)) return @mysqli_errno($this->link);

            //Devuelve el affected rows:
            return array("affected" => @mysqli_affected_rows($this->link));
        }

        public function update($query) {
            //Intenta efectuar cualquier consulta UPDATE.
            //Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
            //Devuelve las filas afectadas (como array asociativo) o el código del error que se produzca.
            @mysqli_multi_query($this->link, $query);
            //Si hubo error, devuelve su código, aunque como valor negativo, para que no se confunda con el AFFECTED ROWS:
            if (@mysqli_errno($this->link)) return @mysqli_errno($this->link);

            //Devuelve el affected rows:
            return array("affected" => @mysqli_affected_rows($this->link));
        }
        
        public function other($query): int {
            //Intenta efectuar cualquier consulta SQL, que no retribuya un resultset, un insert id o affected rows.
            //Ejecuta multiquery, para brindar la posibilidad de emplear consultas asíncronas y múltiples:
            @mysqli_multi_query($this->link, $query);
            return @mysqli_errno($this->link); //Si hubo error, devuelve su código.
        }

        public function escape(string $text): string {
            //Escapa el texto, para evitar inyecciones SQL:
            return @mysqli_real_escape_string($this->link, $text);
        }

        public function ping() {
            //Efectúa un ping a MYSQL, para poder revivir una conexión caída (gone away).
            //Esto requiere de la presencia de la opción "mysqli.reconnect" del php.ini, con el valor "On".
            @mysqli_ping($this->link);
        }
    }
?>